import React, { Component } from 'react';
import './works.css';

class Works extends Component {
  render(){
    return (
      <div>
        <div className='worksTitle'>
          <div className='container'>
            <span className='worksTitle__text'>Our Awesome Works  --</span>
            <a href="#" className='btn worksBtn'>Filter Portfolio</a>
          </div>
        </div>
        <div className='works'>
          <div className="container">
            <div className="worksFlexBox">
              <div className="workItem"><a href="#"><img src="images/main1.jpg" alt="work1"/></a></div>
              <div className="workItem"><a href="#"><img src="images/main2.jpg" alt="work2"/></a></div>
              <div className="workItem"><a href="#"><img src="images/main3.jpg" alt="work3"/></a></div>
              <div className="workItem"><a href="#"><img src="images/main4.jpg" alt="work4"/></a></div>
              <div className="workItem"><a href="#"><img src="images/main5.jpg" alt="work5"/></a></div>
              <div className="workItem"><a href="#"><img src="images/main6.jpg" alt="work6"/></a></div>
              <div className="workItem"><a href="#"><img src="images/main7.jpg" alt="work7"/></a></div>
              <div className="workItem"><a href="#"><img src="images/main8.jpg" alt="work8"/></a></div>
              <div className="workItem"><a href="#"><img src="images/main9.jpg" alt="work9"/></a></div>
            </div>
          </div>
        </div>
        <div className='worksContact'>
          <div className="container">
            <span className="contactText">would you love to work with us?</span>
            <a href="#" className='btn'>contact us</a>
          </div>
        </div>
      </div>
    );
  }
}

export default Works;