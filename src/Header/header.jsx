import React, { Component } from 'react';
import './header.css';

class Header extends Component {
  render() {
    return (
      <div>
        <header>
          <div className='container'>
            <div className='headerWrap'>
              <div className='logo'>
                <a href="#"><img src="images/logo.png" alt="Logo"/></a>
              </div>
              <div className='mainNav'>
                <ul>
                  <li><a href="#">Home</a></li>
                  <li><a href="#">About us</a></li>
                  <li><a href="#">Our Works</a></li>
                  <li><a href="#">Journal</a></li>
                  <li><a href="#">Contact</a></li>
                </ul>
              </div>
            </div>
          </div>
          <div className='headerSlogan'>
            <div className='container'>
              <h1>We’re Keepsake, a digital & branding agency</h1>
              <p>Specializing in web design, poster, motion video, based in London. We love to turn ideas into beautiful things.</p>
            </div>
          </div>
        </header>
      </div>
    );
  }
}


export default Header;