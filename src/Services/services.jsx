import React, {Component} from "react";
import './services.css';

class Services extends Component {
  render () {
    return (
      <div>
        <div className='servicesTitle'>
          <div className='container'>
            <span className='servicesTitle__title'>Our Awesome Works  --</span>
            <span className='services-text'>Maecenas sed diam eget risus varius blandit
              sit amet non magna. Morbi leo risus, porta ac consectetur ac, vestibulum at eros.</span>
          </div>
        </div>
        <div className="container">
          <div className="servicesFlexBox">
            {<Cards title='Web Design' text='Donec elit non mi porta gravida eureget metus. Aenean eu leo quam.
            Pellentesque ornare sem por quam venenatis vestibulum.' link='/images/resp.png' />}
            {<Cards title='Motion Video' text='Aenean lacinia bibendum nulla sed consectetur.
            Nullam id dolor id nibh ultricies vehicula ut id elit. Sed posuere consectetur.' link='/images/tv.png' />}
            {<Cards title='Print Design' text='Maecenas sed diam eget risus varius blandit amet non magna.
             Nullam id dolor id nibh ultricies vehiculaelit praesent commodo.' link='/images/print.png' />}
            {<Cards title='Illustration' text='Cum sociis natoque penatibus et  dis parturient montes, nascetur ridiculus mus.
             Morbi leo risus, porta ac consectetur ac at.' link='/images/love.png' />}
          </div>
        </div>
      </div>
    );
  }
}

function Cards(props) {
  return (
    <div className="servicesItem">
      <img src={props.link} alt="what we can do"/>
      <h3 className='servicesItem__title'>{props.title}</h3>
      <p className='servicesItem__text'>{props.text}</p>
    </div>
  )
}

export default Services;