import React, { Component } from 'react';
import './App.css';
import Header from './Header/header';
import Works from './Works/work';
import Services from './Services/services';

class App extends Component {
  render() {
    return (
      <div>
        <Header/>
        <Works />
        <Services />
      </div>
    );
  }
}

export default App;
